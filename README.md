# The SecMOD MILP Framework

The SecMOD MILP framework is a flexible open-source framework for the optimization and life-cycle assessment of mixed-integer linear multi-energy systems that has been developed by members of the EPSE group in the following paper:

> [Reinert, C., Nolzen, N., Frohmann, J., Tillmanns, D., & Bardow, A. (2023). Design of low-carbon multi-energy systems in the SecMOD framework by combining MILP optimization and life-cycle assessment. Computers & Chemical Engineering, 108176](https://www.sciencedirect.com/science/article/pii/S0098135423000455)

You can find the corresponding Git here: 

https://git-ce.rwth-aachen.de/ltt/secmod-milp
